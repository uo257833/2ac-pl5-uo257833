#include <stdio.h>

int main() {
    double div = (int)3.75;
    int num = 6;
    int result = num / div;
    printf("The variable result is %d, but the division was %f\n", result, num / div);
    return 0;
}