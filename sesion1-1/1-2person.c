#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
    Person Peter;
	PPerson pPeter;
    strcpy(Peter.name, "Peter");
	pPeter=&Peter;
    Peter.heightcm = 175;

    // Assign the weight
	Peter.weightkg = 78.7;
	pPeter->weightkg = 79;

    // Show the information of the Peter data structure on the screen�
	printf("name of the person: '%s'\nheight of the person:'%d cm'\nweigth of the person:'%f kg'\n"
		,Peter.name,Peter.heightcm,Peter.weightkg);

    return 0;
}