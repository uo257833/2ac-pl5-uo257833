	.code

main:
	daddi r1, r2, 5 //movemos la operacion de daddi r1 al principio para poner el xor lo mas atras posible
	xor   r1, r5, r3 //movemos el xor que generaba el problema a la segunda instruccion
	dadd  r6, r4, r2

	dsub  r8, r3, r2

	ld    r8, 120(r1)
	and   r8, r4, r1
	halt
