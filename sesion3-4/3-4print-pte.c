#include <stdio.h>
#include <atc/linmem.h>

#include "3-4print-pte.h"

// Given a virtual address, this virtual address is displayed on
// the screen, as well as its associated Page Table Entry, physical address
// and the flags associated to the Memory Page assigned. On top of this
// information, the title passed as argument is also displayed.
void print_virtual_physical_pte(void *virtual_addr, char *title)
{
	unsigned int physical_addr;
	unsigned int pte;
	unsigned int flags_vp;

	printf("\n%s\n", title);

	// Retrieve the entry in the page table for the virtual address
	if (get_pte(virtual_addr, &pte))
	{
		perror("Linmem module error");
		return;
	}

	// Is there PTE?
	if (pte)
	{ // OK
		// Store the flags associated with the memory page
		flags_vp = <<Complete 1>>

		// Calculate the memory physical address
		physical_addr = <<Complete 2>>
		printf("Virtual address: \t%.8Xh\n"
				"Page Table Entry:\t%.8Xh\n"
				"Physical Address:\t%.8Xh\n"
				"Flags Virtual Page:\t%.3Xh\n",
				(unsigned int)virtual_addr, pte, physical_addr, flags_vp);
	}
	else
	{
		fprintf(stderr, "Page %.5Xh does not have a page table entry\n",
			(unsigned int)virtual_addr >> 12);
	}
}
